const api_url = 'https://vkpjl5qo8g.execute-api.us-east-1.amazonaws.com/dev'
// const api_url = 'https://ch0jz9i3k3.execute-api.us-east-1.amazonaws.com/Prod'
export const environment = {
    production: false,
    hmr       : false,
    api_url: `${api_url}`,
    login_api_url: `/login`,
    user_api_url: `/user`,
    relative_api_url: `/relative`,
    profile_api_url: `/profile`,
    calender_api_url: `/calender`,
    emergency_api_url: `/emergency`,
    caretaker_api: `/caretaker`,
    //TODO: Need to add role IDs Here
    PATIENT:4,
    SUPERADMIN:1,
    ADMIN  :2,
    RELATIVE:5,
    CARETAKER:3,
    DEFAULT_ITERESTS : "Old Age Healthcare",
    logo : "assets/images/logos/jewish_logo.png"
};
