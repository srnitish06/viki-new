// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// const api_url = 'https://ch0jz9i3k3.execute-api.us-east-1.amazonaws.com/Prod'
const api_url = 'https://vkpjl5qo8g.execute-api.us-east-1.amazonaws.com/dev'
export const environment = {
    production: false,
    hmr       : false,
    api_url: `${api_url}`,
    login_api_url: `/login`,
    user_api_url: `/user`,
    relative_api_url: `/relative`,
    profile_api_url: `/profile`,
    calender_api_url: `/calender`,
    emergency_api_url: `/emergency`,
    caretaker_api: `/caretaker`,
    //TODO: Need to add role IDs Here
    PATIENT: 4,
    SUPERADMIN: 1,
    ADMIN: 2,
    RELATIVE: 5,
    CARETAKER: 3,
    DEFAULT_ITERESTS : "Old Age Healthcare",
    logo : "assets/images/logos/jewish_logo.png"


    // api_url: 'https://5argib1jf0.execute-api.us-east-1.amazonaws.com/'
    
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
