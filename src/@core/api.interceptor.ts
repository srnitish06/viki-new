import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { Http } from '@angular/http';
import { UtilsService } from '../app/shared/common-methods';


@Injectable()
export class APIInterceptor implements HttpInterceptor {

    constructor(
        private httpService: Http,
        private UtilService: UtilsService
    ) { }

    private _requestCount = 0;
    // debugger;
    // private _hideLoader(): void {
    //     setTimeout(function (): void { document.getElementById('initalLoading').style.display = 'none'; }, 500);
    // }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> { 
        this._requestCount++;
        return next.handle(request.clone({})).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // Do stuff with response if you want
                if (0 === this._requestCount) {
                    // this._hideLoader();
                }
                return event.body;
            }
        }, (err: any) => {
            this._requestCount--;
            if (0 === this._requestCount) {
                // this._hideLoader();
            }
            let errorMessage: String;
            if (err instanceof HttpErrorResponse) {
                switch (err.status) {
                    case 400: errorMessage = err.error.message; break;
                    case 401: errorMessage = err.error.message; break;
                    case 403: errorMessage = 'Forbidden'; break;
                    case 404: errorMessage = 'Not found'; break;
                    case 422: errorMessage = err.error.errors[Object.keys(err.error.errors)[0]][0]; break;
                    case 423: errorMessage = err.error.message; break;
                    case 429: errorMessage = 'Something went wrong, please refresh the page.'; break;
                    case 500: errorMessage = err.error.message; break;
                    case 503: errorMessage = 'Service unavailable'; break;
                    case 504: errorMessage = 'Gateway Timeout Error, please try again later '; break;
                }
                this.UtilService.displayErrorResponseMessage(errorMessage);
            }
        }, () => {
            this._requestCount--;
            if (0 === this._requestCount) {
                // this._hideLoader();
            }
        });
    }
}
