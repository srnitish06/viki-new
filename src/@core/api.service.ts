import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }

  private _showLoader(): void {
    document.getElementById('initalLoading').style.display = 'block';
  }

  private _getHeadersConfig(isPostX = false): any {
    const config = { 'Authorization': `${localStorage.getItem('token')}` };
    if (!isPostX) {
      config['Accept'] = 'application/json';
      config['Content-Type'] = 'application/json';
    }
    return config;
  }

  private _getDownloadHeadersConfig(): any {
    const config = { 'Authorization': `${localStorage.getItem('token')}` };
    config['Accept'] = 'application/json';
    config['Content-Type'] = 'application/json';
    config['responseType'] = 'arraybuffer';
    return config;
  }


  getUrlParams(params): HttpParams {
    let httpParams = new HttpParams();
    if (params) {
      for (let key in params) {
        httpParams = httpParams.set(key, params[key]);
      }
    }
    return httpParams;

  }

  get(path: string, params?): any {
    params = this.getUrlParams(params)
    return this.http.request('get', `${environment.api_url}${path}`, { params: params, headers: new HttpHeaders(this._getHeadersConfig()) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

  put(path: string, body: Object = {}): any {
    this._showLoader();
    return this.http.put(`${environment.api_url}${path}`, JSON.stringify(body), { headers: new HttpHeaders(this._getHeadersConfig()) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

  post(path: string, body: Object = {}): Observable<any> {
    this._showLoader();
    return this.http.post(`${environment.api_url}${path}`, JSON.stringify(body), { headers: new HttpHeaders(this._getHeadersConfig()) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

  postX(path: string, requestParams: Object = {}): Observable<any> {
    this._showLoader();
    const fd = new FormData();
    for (const key in requestParams) {
      if (requestParams[key]) {
        if (requestParams[key].constructor === Array) {
          for (const innerKey in requestParams[key]) {
            if (requestParams[key][innerKey]) {
              fd.append(key, requestParams[key][innerKey]);
            }
          }
        } else {
          fd.append(key, requestParams[key]);
        }
      } else {
        fd.append(key, requestParams[key]);
      }
    }
    return this.http.post(`${environment.api_url}${path}`, fd, { headers: new HttpHeaders(this._getHeadersConfig(true)) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

  download(path: string, params: URLSearchParams = new URLSearchParams()): any {
    this._showLoader();
    return this.http.request('get', `${environment.api_url}${path}${this.getUrlParams(params)}`, { headers: new HttpHeaders(this._getDownloadHeadersConfig()) })
      .map(
        (res) => res,
        (err) => { err.json(); },
      );
  }

  delete(path: string, body: Object = {}): Observable<any> {
    this._showLoader();
    return this.http.request('delete', `${environment.api_url}${path}`, { body: JSON.stringify(body), headers: new HttpHeaders(this._getHeadersConfig()) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

  customDelete(path: string, body: Object = {}): Observable<any> {
    this._showLoader();
    return this.http.request('get', `${environment.api_url}${path}`, { body: JSON.stringify(body), headers: new HttpHeaders(this._getHeadersConfig()) }).map(
      (res) => res,
      (err) => err.json(),
    );
  }

}
