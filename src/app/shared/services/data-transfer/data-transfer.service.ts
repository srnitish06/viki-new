import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'; 



@Injectable({
  providedIn: 'root' 
})
export class DataTransferService {

  private messageSource = new Subject();
  public currentMessage = this.messageSource.asObservable();

  private userDetailsSource = new Subject();
  public userDetailsSourceMessage = this.userDetailsSource.asObservable();


  constructor() { }

  getHospitalList(message: any) {
    this.messageSource.next(message)
  }

  getPatientList(message: any) {
    this.messageSource.next(message)
  }

  getCareTakerList(message: any) {
    this.messageSource.next(message)
  }

  getDeviceList(message: any){
    this.messageSource.next(message)
  }

  refreshLocalStorageData(message: any) {
    this.messageSource.next(message);
  }

  getRelativeList(message: any) {
    this.messageSource.next(message)
  }
  changeUserDetails(message: any) {
    this.userDetailsSource.next(message)
  }


  
}
