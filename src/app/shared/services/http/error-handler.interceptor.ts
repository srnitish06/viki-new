import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UtilsService } from '../../../shared/common-methods';


/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    constructor(private router: Router, private _utilService: UtilsService) { }
    
        // private _hideLoader(): void {
        //     setTimeout(function (): void { document.getElementById('initalLoading').style.display = 'none'; }, 500);
        // }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // this._hideLoader();
        return next.handle(request).pipe(catchError(error => this.errorHandler(error)));
    }

    // Customize the default error handler here if needed
    private errorHandler(response: HttpEvent<any>): Observable<HttpEvent<any>> {
        if (response['status'] == 401 || response['status'] == 0) {
            this.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
        }
        throw response;
    }
    logout(): Observable<boolean> {
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        return of(true);
    }
}
