import { Inject, Injectable, InjectionToken, Injector, Optional } from '@angular/core';
import { HttpClient, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';


// From @angular/common/http/src/interceptor: allows to chain interceptors
class HttpInterceptorHandler implements HttpHandler {
  
  constructor(private next: HttpHandler, private interceptor: HttpInterceptor) { }

  handle(request: HttpRequest<any>): Observable<HttpEvent<any>> {
    return this.interceptor.intercept(request, this.next);
  }
}

export const HTTP_DYNAMIC_INTERCEPTORS = new InjectionToken<HttpInterceptor>('HTTP_DYNAMIC_INTERCEPTORS');

@Injectable({
  providedIn: 'root'
})
export class HttpService  {

  constructor(private http: HttpClient,
    private httpHandler: HttpHandler,
    private injector: Injector,
    @Optional() @Inject(HTTP_DYNAMIC_INTERCEPTORS) private interceptors: HttpInterceptor[] = []
    ) {

      if (!this.interceptors) {
        // Configure default interceptors that can be disabled here
        this.interceptors = [this.injector.get(ErrorHandlerInterceptor)];
      }
    }

  private _getHeadersConfig(isPostX = false) {
    const config = {};
    if (!isPostX) {
      config['Accept'] = 'application/json, text/plain, */*';
      config['Content-Type'] = 'application/json; charset=UTF-8';
      if (localStorage.getItem('token')) {
        config['Authorization'] = localStorage.getItem('token');
      }
    }
    return config;
  }

  private getUrlParams(params) {
    let paramText = '';
    paramText = Object.keys(params).length > 0 ? '?' : '';
    for (const key in params) {
      if (params.hasOwnProperty(key) && params[key]) {
        paramText += `${key}=${params[key]}&`;
      }
    }
    return paramText;
  }


  get(path: string, params: URLSearchParams = new URLSearchParams()) {
    const handler = this.interceptors.reduceRight(
      (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
      this.httpHandler
    );
    return new HttpClient(handler)
      .get(`${environment.api_url}${path}${this.getUrlParams(params)}`, { headers: new HttpHeaders(this._getHeadersConfig()) })
      .pipe(map(data => data));
  }
  

  getMock(path: string, params: URLSearchParams = new URLSearchParams()) {
    const handler = this.interceptors.reduceRight(
      (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
      this.httpHandler
    );
    return new HttpClient(handler)
      .get(`${path}${this.getUrlParams(params)}`, { headers: new HttpHeaders(this._getHeadersConfig()) })
      .pipe(map(data => data));
  }

  postX(path: string, requestParams: Object = {}): Observable<any> {
    const handler = this.interceptors.reduceRight(
      (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
      this.httpHandler
    );
    const httpHeaders = new HttpHeaders(this._getHeadersConfig());
    return new HttpClient(handler)
      .post(`${environment.api_url}${path}`, requestParams, { headers: httpHeaders })
      .pipe(map(data => data));
  }
  putX(path: string, requestParams: Object = {}): Observable<any> {
    const handler = this.interceptors.reduceRight(
      (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
      this.httpHandler
    );
    const httpHeaders = new HttpHeaders(this._getHeadersConfig());
    return new HttpClient(handler)
      .put(`${environment.api_url}${path}`, requestParams, { headers: httpHeaders })
      .pipe(map(data => data));
  }
  delete(path: string): Observable<any> {
    const handler = this.interceptors.reduceRight(
      (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
      this.httpHandler
    );
    const httpHeaders = new HttpHeaders(this._getHeadersConfig());
    return new HttpClient(handler)
      .delete(`${environment.api_url}${path}`, { headers: httpHeaders })
      .pipe(map(data => data));
  }
}
