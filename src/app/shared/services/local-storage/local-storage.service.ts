import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }


  /*
    * Author: T0518
    * Function: setItem
    * Description:  Function to set the browser localstorage with a key value pair
    * Arguments:
    *     Param 1: key              : String : The key to which the value is to be set in local storage
    *     Param 2: value            : String : Value to be set for the key in localstorage
    *     Param 3: enableEncryption : Boolean : boolean to determine whether the value to se set in encrypted form or not
    * Return: Nil
    */
  public setItem(key: string, value: string, enableEncryption?: boolean) {
    enableEncryption = (enableEncryption !== undefined) ? enableEncryption : false;
    if (enableEncryption) {
      value = this.encryptData(value);
    }
    window.localStorage.setItem(key, value);
  }

  /*
   * Author: T0518
   * Function: setItem
   * Description:  Function to set the browser localstorage with a key value pair
   * Arguments:
   *     Param 1: key              : String : The key to which the value is to be read from local storage
   *     Param 2: enableEncryption : Boolean : boolean to determine whether the value to se set in encrypted form or not
   * Return: Returns the value corresponding to the key from local storage
   */
  public getItem(key: string, enableEncryption?: boolean) {
    enableEncryption = (enableEncryption !== undefined) ? enableEncryption : false;
    let storedValue = window.localStorage.getItem(key);
    if (enableEncryption) {
      storedValue = this.decryptData(storedValue);
    }
    return storedValue;
  }

  /*
   * Author: T0518
   * Function: removeItem
   * Description:  Function to remove an item from local storage
   * Arguments:
   *     Param 1: key : String : The key to which the value is to be removed from local storage
   * Return: Nil
   */
  public removeItem(key: string) {
    window.localStorage.removeItem(key);
  }

  /*
   * Author: T0518
   * Function: clear
   * Description:  Function to clear local storage
   * Arguments: Nil
   * Return: Nil
   */
  public clear() {
    window.localStorage.clear();
  }

  /*
   * Author: T0518
   * Function: encryptData
   * Description:  Function to set encrypt the data inside localstorage befor setting
   * Arguments:
   *     Param 1: value : String : The value to be encrypted
   * Return: Returns encrypted value of the input
   */
  private encryptData(value) {
    return value;
  }

  /*
   * Author: T0518
   * Function: encryptData
   * Description:  Function to set encrypt the data inside localstorage befor setting
   * Arguments:
   *     Param 1: value : String : The value to be encrypted
   * Return: Returns encrypted value of the input
   */
  private decryptData(value) {
    return value;
  }



}
