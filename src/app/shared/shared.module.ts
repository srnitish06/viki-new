import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from './common-methods';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    UtilsService,
  ]
})
export class SharedModule { }
