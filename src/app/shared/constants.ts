export class Constants {
    
    public static get USER_ROLES(): any {
        return {
            HOSPITAL: '2',
            PATIENT: '4',
            RELATIVE: '5',
            CARETAKER: '3',
            SUPER_ADMIN: '1',
        };
    }
}
