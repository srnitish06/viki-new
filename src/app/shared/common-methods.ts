import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../shared/constants';

@Injectable()

export class UtilsService {
    constructor(
        private toastrService: ToastrService
    ) { }

    public getUserDetails(): any {
        let userData = {};
        let dummyData = {};
        userData = JSON.parse(localStorage.getItem('user_details')) ? JSON.parse(localStorage.getItem('user_details')) : {};
        if (userData === {}) {
            dummyData = {
                name: 'Airo',
                phone: '+1205628638912',
                email: 'airo.digitallab@gmail.com',
                emergency_phone: '+3629237923'
            };
            userData = dummyData;
        }
        return userData;
    }

    public displaySuccessResponseMessage(responseMessage): void {
        this.toastrService.success(responseMessage);
    }

    public displayErrorResponseMessage(responseMessage): void {
        this.toastrService.error(responseMessage);
    }

    public setEnvironmentPath(userRole): string {
        let path = '';
        switch (userRole) {
            case Constants.USER_ROLES.SUPER_ADMIN:
                path = 'superadmin';
                break;
            case Constants.USER_ROLES.PATIENT:
                path = 'patient';
                break;
            case Constants.USER_ROLES.RELATIVE:
                path = 'relative';
                break;
            case Constants.USER_ROLES.CARETAKER:
                path = 'caretaker';
                break;
            case Constants.USER_ROLES.HOSPITAL:
                path = 'hospital';
                break;
        }
        return path;
    }

    public calculateAge(date): any {
        const dateString = this.formatDate(date);
        const today = new Date();
        const birthDate = new Date(dateString);
        let age = 0;
        age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        if (age > 0) {
            return age;
        } else {
            return '';
        }
    }

    public formatDate(date): any {
        let formattedDate = '';
        if ('' !== date) {
            formattedDate = new Date(date).toISOString();
        }
        return formattedDate;
    }

    public hideLoader(): void {
        setTimeout(
            function (): void {
                if (document.getElementById('initalLoading')) {
                    document.getElementById('initalLoading').style.display = 'none';
                }
            }, 500);
    }

}
