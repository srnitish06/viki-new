// import { Injectable } from '@angular/core';
// import { HttpService } from '../shared/services/http/http.service';
// import { UtilsService } from '../shared/common-methods';
// import { Observable } from 'rxjs/Observable';
// import { url } from 'inspector';
// import { HttpParams } from '@angular/common/http';
// @Injectable({
//   providedIn: 'root'
// })
// export class AiropagesService {

//   constructor(private httpService: HttpService, _utilservice: UtilsService) { }

//   public _showLoader(): void {
//     document.getElementById('initalLoading').style.display = 'block';
//   }
//   getUrlParams(params): HttpParams {
//     let httpParams = new HttpParams();
//     if (params) {
//       for (let key in params) {
//         httpParams = httpParams.set(key, params[key]);
//       }
//     }
//     return httpParams;

//   }
//   public getUserDetails(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getMedicationRemainder(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.get(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getDictationRemainder(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getPatientList(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public addCareTaker(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getAllCaretakerList(url, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.get(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }

//   public deleteCareTaker(url, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.delete(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }

//   public searchCareTaker(url, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.get(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }



//   public addPatient(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getAllPatientList(url, cbSuccess, cbError, cid?,showLoader?) {
//     this._showLoader();
//     if (cid) {
//       url = `${url}?cid=${cid}`;
//     }
//     this.httpService.get(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }
//   public getCaretakersOfPatient(url, cbSuccess, cbError, pid?, showLoader?) {
//     this._showLoader();
//     if (pid) {
//       url = `${url}?pid=${pid}`;
//     }
//     this.httpService.get(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }
//   public deletePatient(url, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.delete(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }

//   public updateProfile(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.postX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getAllVitalReportList(url, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.get(url).subscribe(result => {
//       cbSuccess(result);
//     }, error => cbError(error)
//     );
//   }

//   public updateDictationNote(url, params, cbSuccess, cbError, showLoader?) {
//     this._showLoader();
//     this.httpService.putX(url, params)
//       .subscribe(result => {
//         cbSuccess(result);
//       },
//         error => cbError(error)
//       );
//   }

//   public getDeviceList(path): Observable<any> {
//     // const finalPath = 'beta/' + roleBasedPath + '/' + 'profile';
//     this._showLoader();
//     return this.httpService.get(`${path}`).map(data => data, err => { console.log(err); });
//   }

//   public saveDeviceDetails(path, data): Observable<any> {
//     return this.httpService.postX(`${path}`, data).map(data => data, err => { console.log(err); });
//   }

//   public mapDevice(path, data): Observable<any> {
//     return this.httpService.putX(`${path}`, data).map(data => data, err => { console.log(err); })
//   }
//   public getDeviceMaster() {
//     return this.httpService.get("/master/device-master").map(data => data, err => { console.log(err); });
//   }
//   public removeMapping(mid): Observable<any> {
//     return this.httpService.delete(`/device/mapper?mid=${mid}`).map(data => data, err => { console.log(err); })
//   }
//   public saveADLMapping(data): Observable<any> {
//     this._showLoader();
//     return this.httpService.postX(`/patient/adl`, data).map(data => data, err => { console.log(err); });
//   }
//   public getADLMapping(cid?, pid?) {
//     this._showLoader();
//     let params: any = '';
//     if (cid && pid) {
//       params = this.getUrlParams({ cid: cid, pid: pid })
//     }
    
//     return this.httpService.get(`/patient/adl?${params}`).map(data => data, err => { console.log(err); });
//   }
//   public getADLMaster() {
//     this._showLoader();
//     return this.httpService.get(`/master/adl-master`).map(data => data, err => { console.log(err); });
//   }

//   public getADLReportList() {
//     this._showLoader();
//     return this.httpService.get(`/adl`).map(data => data, err => { console.log(err); });
//   }
//   public downloadReport(cid, pid, y, w) {
//     return this.httpService.get(`/adl?cid=${cid}&pid=${pid}&y=${y}&w=${w}`).map(data => data, err => { console.log(err); });
//   }
//   public getCaretakerVisitLog() {
//     this._showLoader();
//     return this.httpService.get(`/caretaker/visit-log `).map(data => data, err => { console.log(err); });
//   }
// }
