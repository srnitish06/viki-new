import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AiropagesRoutingModule } from './airopages-routing.module';
// import { CreateUserComponent } from './patient/create-user/create-user.component';
// import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { ReactiveFormsModule } from '@angular/forms';
// import { AddHospitalFormComponent } from './hospital/add-hospital-form/add-hospital-form.component';
// import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
// import { MatSelectModule } from '@angular/material/select';
// import { DeleteHospitalFormComponent } from './hospital/delete-hospital-form/delete-hospital-form.component';
// import { DeletePatientFormComponent } from './patient/delete-patient-form/delete-patient-form.component';
// import { DeleteCareTakerFormComponent } from './caretaker/delete-caretaker-form/delete-caretaker-form.component';
// import { MyProfileModule } from './my-profile/my-profile.module';
// import { AdlDetailsComponent } from './patient/adl-details/adl-details.component';
import { FormsModule } from '@angular/forms';
// import { CaretakerComponent } from './caretaker/caretaker.component';
// import { LoginComponent } from '../@auth/login/login.component';
// import { PatientComponent } from './patient/patient.component';
// import { AddPatientFormComponent } from './patient/add-patient-form/add-patient-form.component';
// import { UserCreateFormComponent } from './caretaker/user-create/user-create-form.component';
// import { AddCaretakerComponent } from './add-caretaker/add-caretaker.component';
// import { AddCaretakerFormComponent } from './caretaker/add-caretaker-form/add-caretaker-form.component';
@NgModule({
  declarations: [
    // LoginComponent,
    // PersonalInfoComponent,
    // AddHospitalFormComponent,
    // DeleteHospitalFormComponent,
    // DeletePatientFormComponent,
    // DeleteCareTakerFormComponent,
    // AdlDetailsComponent
  // PatientComponent,
  //   AddPatientFormComponent
  // CreateUserComponent
// CaretakerComponent
// CreateCaretakerComponent
],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    FormsModule,
    CommonModule,
    AiropagesRoutingModule,
    ReactiveFormsModule,
    // MyProfileModule,
    // InternationalPhoneNumberModule
  ],
  entryComponents: [
    // PersonalInfoComponent,
    // AddHospitalFormComponent,
    // DeleteHospitalFormComponent,
    // DeletePatientFormComponent,
    // DeleteCareTakerFormComponent,
    // AdlDetailsComponent
  ]
})
export class AiropagesModule { }
