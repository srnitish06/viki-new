import { TestBed } from '@angular/core/testing';

import { AiropagesService } from './airopages.service';

describe('AiropagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AiropagesService = TestBed.get(AiropagesService);
    expect(service).toBeTruthy();
  });
});
