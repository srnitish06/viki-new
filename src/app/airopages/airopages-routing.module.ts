import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../airopages/dashboard/dashboard.component';
import { LoginComponent } from '../@auth/login/login.component';
// import { AiropagesComponent } from './airopages.component';
import { PagenotfoundComponent } from '../airopages/pagenotfound/pagenotfound.component';
import { PatientComponent} from '../airopages/patient/patient.component';
import { AddPatientFormComponent } from '../airopages/patient/add-patient-form/add-patient-form.component'
import { CreateUserComponent } from '../airopages/patient/create-user/create-user.component';
import { CaretakerComponent } from '../airopages/caretaker/caretaker.component';
import { CreateCaretakerComponent } from '../airopages/caretaker/create-caretaker/create-caretaker.component';
import { AddCaretakerFormComponent } from '../airopages/caretaker/add-caretaker-form/add-caretaker-form.component';
import { DictationReportComponent } from '../airopages/caretaker/dictation-report/dictation-report.component';
import { CalendarComponent } from '../airopages/calendar/calendar.component';
import { ForgotPasswordComponent } from '../airopages/forgot-password/forgot-password.component';

import { AuthService } from '../auth.service';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  { path: "", component: DashboardComponent },
  { path: "Dashboard", component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'patient', component: PatientComponent },
  { path: 'patient/add-patient-form', component: AddPatientFormComponent},
  { path: 'patient/create-user', component: CreateUserComponent},
  { path: 'caretaker', component: CaretakerComponent },
  { path: 'caretaker/create-caretaker', component: CreateCaretakerComponent },
  { path: 'caretaker/add-caretaker-form', component: AddCaretakerFormComponent},
  { path: 'dictation-report', component: DictationReportComponent},
  { path: 'calendar', component: CalendarComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent},
  { path: "**", component: PagenotfoundComponent }, 

];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
//   declarations: [
//     AiropagesComponent,
//   ],
  providers: [AuthService, AuthGuard],
})

export class AiropagesRoutingModule { }

