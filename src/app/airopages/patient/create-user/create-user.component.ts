import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  first_name: string;
  last_name: string;
  email: string;
  mrn: number;
  phone: number;
  gender: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {mrn: 1, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 2, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 3, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 4, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 5, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 6, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 7, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 8, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 9, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {mrn: 10, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
];


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  displayedColumns: string[] = ['mrn', 'first_name', 'last_name', 'email', 'phone', 'gender', 'action'];
  dataSource = ELEMENT_DATA;

  length = 10;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 20];
  constructor() { }

  ngOnInit() {
  }

}
