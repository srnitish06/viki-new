import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiropagesComponent } from './airopages.component';

describe('AiropagesComponent', () => {
  let component: AiropagesComponent;
  let fixture: ComponentFixture<AiropagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiropagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiropagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
