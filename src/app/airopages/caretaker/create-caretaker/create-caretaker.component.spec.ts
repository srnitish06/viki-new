import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCaretakerComponent } from './create-caretaker.component';

describe('CreateCaretakerComponent', () => {
  let component: CreateCaretakerComponent;
  let fixture: ComponentFixture<CreateCaretakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCaretakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCaretakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
