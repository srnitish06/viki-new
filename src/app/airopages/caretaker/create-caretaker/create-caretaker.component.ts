import { Component, OnInit , ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material';

export interface PeriodicElement {
  alexa_login_id: number;
  first_name: string;
  last_name: string;
  email: string;
  phone: number;
  status: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {alexa_login_id: 1, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'Active', action:'Edit'},
  {alexa_login_id: 2, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'InActive', action:'Edit'},
  {alexa_login_id: 3, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'Active', action:'Edit'},
  {alexa_login_id: 4, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'InActive', action:'Edit'},
  {alexa_login_id: 5, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'Active', action:'Edit'},
  {alexa_login_id: 6, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'InActive', action:'Edit'},
  {alexa_login_id: 7, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'Active', action:'Edit'},
  {alexa_login_id: 8, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'InActive', action:'Edit'},
  {alexa_login_id: 9, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'Active', action:'Edit'},
  {alexa_login_id: 10, first_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, status:'InActive', action:'Edit'},
  
];

@Component({
  selector: 'app-create-caretaker',
  templateUrl: './create-caretaker.component.html',
  styleUrls: ['./create-caretaker.component.css']
})
export class CreateCaretakerComponent implements OnInit {

  displayedColumns: string[] = ['alexa_login_id', 'first_name', 'last_name', 'email', 'phone', 'status', 'action'];
  dataSource = ELEMENT_DATA;
  public searchText: string;



  length = 10;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 20];
  
  constructor() { }

  ngOnInit() {
  }


}
