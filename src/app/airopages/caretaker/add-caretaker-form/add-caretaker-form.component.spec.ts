import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCaretakerFormComponent } from './add-caretaker-form.component';

describe('AddCaretakerFormComponent', () => {
  let component: AddCaretakerFormComponent;
  let fixture: ComponentFixture<AddCaretakerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCaretakerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCaretakerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
