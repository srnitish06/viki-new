import { Component, OnInit } from '@angular/core';



export interface PeriodicElement {
  report_id: number;
  report_name: string;
  report_date: any;
  patient_name: string;
  last_name: string;
  email: string;
  phone: number;
  gender: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
  {report_id:1234, report_name:'Heart Report', report_date: 12062019, patient_name: 'Kumar', last_name: 'Alok', email: 'sr.alok@gmail.com', phone: 9599192072, gender:'Male', action:'Add'},
 
];

@Component({
  selector: 'app-dictation-report',
  templateUrl: './dictation-report.component.html',
  styleUrls: ['./dictation-report.component.css']
})


export class DictationReportComponent implements OnInit {

  displayedColumns: string[] = ['report_id','report_name','patient_name', 'report_date', 'action'];
  dataSource = ELEMENT_DATA;
  length = 10;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 20];

  constructor() { }

  ngOnInit() {
  }

}
