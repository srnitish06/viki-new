import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DictationReportComponent } from './dictation-report.component';

describe('DictationReportComponent', () => {
  let component: DictationReportComponent;
  let fixture: ComponentFixture<DictationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DictationReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DictationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
