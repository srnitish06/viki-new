import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { AppComponent } from './app.component';
import 'hammerjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { ErrorHandlerInterceptor } from './shared/services/http/error-handler.interceptor';
import { ApiService } from '../@core/api.service';
// import { APIInterceptor } from '../@core/api.service';
import { TranslateModule } from '@ngx-translate/core';
import { DataTransferService } from './shared/services/data-transfer/data-transfer.service';
import { SharedModule } from './shared/shared.module';
// import { LayoutModule } from 'app/layout/layout.module';
import { DashboardComponent } from './airopages/dashboard/dashboard.component';
import { AiropagesComponent } from './airopages/airopages.component';
import { HeaderComponent } from './airopages/header/header.component';
import { SidenavComponent } from './airopages/sidenav/sidenav.component';
import { FooterComponent } from './airopages/footer/footer.component';
import { LoginComponent } from './@auth/login/login.component';
import { PatientComponent } from './airopages/patient/patient.component';
import { AddPatientFormComponent } from './airopages/patient/add-patient-form/add-patient-form.component'
import { CreateUserComponent } from  './airopages/patient/create-user/create-user.component'
import { PagenotfoundComponent } from './airopages/pagenotfound/pagenotfound.component';
import { CaretakerComponent } from './airopages/caretaker/caretaker.component';
import { CreateCaretakerComponent } from './airopages/caretaker/create-caretaker/create-caretaker.component';
import { AddCaretakerFormComponent } from './airopages/caretaker/add-caretaker-form/add-caretaker-form.component';
import { DictationReportComponent } from './airopages/caretaker/dictation-report/dictation-report.component';
import { CalendarComponent } from './airopages/calendar/calendar.component';
import { ForgotPasswordComponent } from './airopages/forgot-password/forgot-password.component';

import { AiropagesModule } from './airopages/airopages.module';
import { MaterialModule } from './material.module';


const appRoutes: Routes = [
  {
      path: 'pages',
      loadChildren: './main/sample/sample.module#SampleModule',
  },
  {
      path: 'login',
      // loadChildren: './@auth/login-2/login-2.module#Login2Module'
      loadChildren: ''
  },
  // {
  //     path: 'dictation-report',
  //     loadChildren: './airopages/dictation-report/dictation-report.module#DictationReportModule'
  // },
  // {
  //     path: 'dictation-report-details/:date',
  //     loadChildren: './airopages/dictation-report/dictation-report-details/dictation-report-details.module#DictationReportDetailsModule',
  // },
  {
      path: 'vital-report',
      loadChildren: './airopages/demopage/demopage.module#DemoPageModule'
  },
  {
      path: 'vital-details/:date',
      loadChildren: './airopages/demodetails/demodetails.module#DemodetailsModule',
  },
  {
      path: 'forgot-password',
      loadChildren: './airopages/forgot-password/forgot-password.module#ForgotPasswordModule',
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AiropagesComponent,
    FooterComponent,
    HeaderComponent,
    SidenavComponent,
    LoginComponent,
    PagenotfoundComponent,
    AddPatientFormComponent,
    PatientComponent,
    CaretakerComponent,
    CreateCaretakerComponent,
    AddCaretakerFormComponent,
    CreateUserComponent,
    DictationReportComponent,
    ForgotPasswordComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, config),
    TranslateModule.forRoot(),
    SharedModule,
    MaterialModule,
  //   LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    AiropagesModule,
    ToastrModule.forRoot({
    timeOut: 4000,
    positionClass: 'toast-top-right',
    preventDuplicates: true,
    closeButton: true,
  }),
  ],
  providers: [ApiService, ErrorHandlerInterceptor, DataTransferService],
  bootstrap: [AppComponent]
})

export class AppModule { }
