import { Injectable } from '@angular/core';
import { HttpService } from './shared/services/http/http.service';
import { environment } from '../environments/environment';
 
@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpService: HttpService) { }
  
  public _showLoader(): void {
    document.getElementById('initalLoading').style.display = 'block';
  }

  public loginUser( params, cbSuccess, cbError, showLoader?) {
    this.httpService.postX(environment.login_api_url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }

  public forgotpassword(url, params, cbSuccess, cbError, showLoader?) {
    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }

  public getAllVitalReportList(url, cbSuccess, cbError, showLoader?) {
    this.httpService.get(url).subscribe(result => {
      cbSuccess(result);
    }, error => cbError(error)
    );
  }
  public getActivityLogs(url, cbSuccess, cbError, showLoader?) {
    this._showLoader();
    this.httpService.get(url).subscribe(result => {
      cbSuccess(result);
    }, error => cbError(error)
    );
  }
}
