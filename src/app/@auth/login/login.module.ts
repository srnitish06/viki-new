import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { FuseSharedModule } from '@fuse/shared.module';
// import { LoginComponent } from './login-2.component';
import { LoginComponent } from '../login/login.component';


const routes = [
    { path : '', component: LoginComponent }
];

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        // MatButtonModule,
        // MatCheckboxModule,
        // MatFormFieldModule,
        // MatIconModule,
        // MatInputModule,
        // MatProgressBarModule,
        // FuseSharedModule
    ]
})

export class LoginModule {}
